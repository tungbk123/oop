<?php

interface ShopProductInterface
{
    /**
     * Get information
     *
     * @return array
     */
    public function getInformation(): array;
}