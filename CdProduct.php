<?php
require_once('ShopProduct.php');

class CdProduct extends ShopProduct
{
    private $length;

    public function __construct(string $name, string $author, float $price, float $length)
    {
        parent::__construct($name, $author, $price);
        $this->length = $length;
    }

    public function getInformation(): array
    {
        $data = parent::getInformation();
        $data['length'] = $this->length;
    }

}