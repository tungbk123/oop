<?php
require_once('ShopProductInterface.php');

class ShopProduct implements ShopProductInterface
{
    private $name;
    private $author;
    private $price;

    /**
     * ShopProduct constructor.
     *
     * @param string $name name product
     * @param string $author author product
     * @param float $price price product
     */
    public function __construct(string $name, string $author, float $price)
    {
        $this->name = $name;
        $this->author = $author;
        $this->price = $price;
    }

    /**
     * Get name
     *
     * @return string
     */
    protected function getName(): string
    {
        return $this->name;
    }

    /**
     * Get author
     *
     * @return string
     */
    protected function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * Get price
     *
     * @return float
     */
    protected function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Get information
     *
     * @return array
     */
    public function getInformation(): array
    {
        return [
            'name' => $this->getName(),
            'author' => $this->getAuthor(),
            'price' => $this->getPrice()
        ];
    }
}
