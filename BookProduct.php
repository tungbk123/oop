<?php
include_once('ShopProduct.php');

class BookProduct extends ShopProduct
{
    private $page;

    /**
     * BookProduct constructor.
     *
     * @param string $name name of book
     * @param string $author author of author
     * @param float $price book price
     * @param int $page page of book
     */
    public function __construct(string $name, string $author, float $price, int $page)
    {
        parent::__construct($name, $author, $price);
        $this->page = $page;
    }

    /**
     * Get information
     *
     * @param int $page
     *
     * @return array
     */
    public function getInformation(): array
    {
        $data = parent::getInformation();
        $data['page'] = $this->page;
        return $data;
    }
}

$a = new BookProduct('B', 'Tung', 12.2003, 10);
$result = $a->getInformation();
var_dump($result);


